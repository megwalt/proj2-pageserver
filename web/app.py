from flask import Flask, render_template, request, url_for, abort
from jinja2.exceptions import TemplateNotFound

app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"


# 403 forbidden file name used
@app.errorhandler(403)
def page_forbidden(error):
    return render_template("403.html"), 403


# 404 file not found (if find forbidden characters redirect to 403)
@app.errorhandler(404)
def page_not_found(error):
    if not request.url.endswith(".html", ".css"):
        return page_forbidden(403)

    return render_template("404.html"), 404


# Standard html or css file
@app.route("/<filepath>")
def page_render(filepath):
    try:
        if '//' in request.__dict__['environ']['REQUEST_URI']:
            abort(403)
        return render_template(filepath)

    except TemplateNotFound:
        abort(404)


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
